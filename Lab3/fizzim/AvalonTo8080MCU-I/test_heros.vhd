-- Created by fizzim.pl version 5.20 on 2016:12:01 at 10:41:53 (www.fizzim.com)

library ieee;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity cliff is
port (
  rd : out STD_LOGIC;
  rs : out STD_LOGIC;
  clk : in STD_LOGIC;
  go : in STD_LOGIC;
  rst_n : in STD_LOGIC;
  ws : in STD_LOGIC
);
end cliff;

architecture fizzim of cliff is

-- state bits
subtype state_type is STD_LOGIC_VECTOR(2 downto 0);

constant Iddle: state_type:="000"; -- extra=0 rs=0 rd=0 
constant DLY: state_type:="001"; -- extra=0 rs=0 rd=1 
constant DONE: state_type:="010"; -- extra=0 rs=1 rd=0 
constant Read: state_type:="101"; -- extra=1 rs=0 rd=1 

signal state,nextstate: state_type;
signal rd_internal: STD_LOGIC;
signal rs_internal: STD_LOGIC;

-- comb always block
begin
  COMB: process(state,go,ws) begin
    -- Warning I2: Neither implied_loopback nor default_state_is_x attribute is set on state machine - defaulting to implied_loopback to avoid latches being inferred 
    nextstate <= state; -- default to hold value because implied_loopback is set
    case state is
      when Iddle =>
        if (go) then
          nextstate <= Read;
        else
          nextstate <= Iddle;
        end if;

      when DLY   =>
        if (ws) then
          nextstate <= Read;
        else
          nextstate <= DONE;
        end if;

      when DONE  =>
        nextstate <= Iddle;

      when Read  =>
        nextstate <= DLY;

      when others =>

    end case;
  end process;

  -- Assign reg'd outputs to state bits
  rd_internal <= state(0);
  rs_internal <= state(1);

  -- Port renames for vhdl
  rd <= rd_internal;
  rs <= rs_internal;

  -- sequential always block
  FF: process(clk,rst_n,nextstate) begin
    if (rst_n='0') then
      state <= Iddle;
    elsif (rising_edge(clk)) then
      state <= nextstate;
    end if;
  end process;
end fizzim;
