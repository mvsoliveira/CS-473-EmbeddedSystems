transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {Z:/CS-473-EmbeddedSystems/Lab2/ParalellPort/ParallelPort/ParallelPort.vhd}

vcom -93 -work work {Z:/CS-473-EmbeddedSystems/Lab2/ParalellPort/ParallelPort/ParallelPort_tb.vhd}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cyclonev -L rtl_work -L work -voptargs="+acc"  ParallelPort_tb

add wave *
view structure
view signals
run 500 ns
