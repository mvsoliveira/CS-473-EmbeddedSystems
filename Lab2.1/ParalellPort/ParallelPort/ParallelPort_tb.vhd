----------------------------------------------------------------------------------------------------------------------
-- Title      : Testbench for design "ParallelPort"
-- Project    : MUCTPI

----------------------------------------------------------------------------------------------------------------------
-- File       : ParallelPort_tb.vhd
-- Author     : Marcos Oliveira
-- Company    : CERN
-- Created    : 2016-11-11
-- Last update: 2016-11-11
-- Platform   : Symplify Premier G-2012.09 and Mentor Modelsim SE-64 10.1c
-- Standard   : VHDL'93/02
----------------------------------------------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2016 CERN
----------------------------------------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-11-11  1.0      msilvaol	Created
----------------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

------------------------------------------------------------------------------------------------------------------------

entity ParallelPort_tb is

end entity ParallelPort_tb;

------------------------------------------------------------------------------------------------------------------------

architecture tb of ParallelPort_tb is

  -- component ports
  signal Clk        : std_logic;
  signal nReset     : std_logic;
  signal Address    : std_logic_vector (2 downto 0);
  signal ChipSelect : std_logic;
  signal Read_en       : std_logic;
  signal Write_en      : std_logic;
  signal ReadData   : std_logic_vector (7 downto 0);
  signal WriteData  : std_logic_vector (7 downto 0);
  signal ParPort    : std_logic_vector (7 downto 0);
  
  constant gbl_clk_period : time    := 20 ns;


begin  -- architecture tb

  -- component instantiation
  DUT: entity work.ParallelPort
    port map (
      Clk        => Clk,
      nReset     => nReset,
      Address    => Address,
      ChipSelect => ChipSelect,
      Read_en       => Read_en,
      Write_en      => Write_en,
      ReadData   => ReadData,
      WriteData  => WriteData,
      ParPort    => ParPort);

  -- clock generation
    gbl_clk : process
  begin
    Clk <= '0';
    wait for gbl_clk_period/2;
    Clk <= '1';
    wait for gbl_clk_period/2;
  end process;
  
  

  -- waveform generation
  WaveGen_Proc: process
  begin
    -- insert signal assignments here
	 nReset <= '0';
	 Address <= (others => '0');
	 ChipSelect <= '0';
	 Write_en <= '0';
	 Read_en <= '0';
	 WriteData <= (others => '0');
	 wait for 20 ns;
	 nReset <= '1';
	 Address <= "000";
	 ChipSelect <= '1';
	 Write_en <= '1';
	 WriteData <= "11111000";
	 wait for 20 ns;
	 Address <= "000";
	 WriteData <= "11111001";
	 wait for 20 ns;
	 Address <= "010";
	 WriteData <= "11111010";
	 wait for 20 ns;
	 Address <= "011";
	 WriteData <= "11111011";
	 wait for 20 ns;
	 Address <= "100";
	 WriteData <= "11111100";
	 wait for 20 ns;
	 Address <= "000";
	 ChipSelect <= '1';
	 Write_en <= '0';
	 Read_en <= '1';
	 wait for 20 ns;
	 Address <= "001";
	 wait for 20 ns;
	 Address <= "010";
    wait;
  end process WaveGen_Proc;

  

end architecture tb;

------------------------------------------------------------------------------------------------------------------------

configuration ParallelPort_tb_tb_cfg of ParallelPort_tb is
  for tb
  end for;
end ParallelPort_tb_tb_cfg;

------------------------------------------------------------------------------------------------------------------------
