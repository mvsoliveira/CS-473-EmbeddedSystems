----------------------------------------------------------------------------------------------------------------------
-- Title      : ParalellPort
-- Project    : MUCTPI

----------------------------------------------------------------------------------------------------------------------
-- File       : paralellport.vhd
-- Author     : Marcos Oliveira
-- Company    : CERN
-- Created    : 2016-11-11
-- Last update: 2016-11-11
-- Platform   : Symplify Premier G-2012.09 and Mentor Modelsim SE-64 10.1c
-- Standard   : VHDL'93/02
----------------------------------------------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2016 CERN
----------------------------------------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-11-11  1.0      msilvaol	Created
----------------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ParallelPort is
  port (
    -- Avalon interfaces signals
    Clk        : in    std_logic;
    nReset     : in    std_logic;
    Address    : in    std_logic_vector (2 downto 0);
    ChipSelect : in    std_logic;
    Read_en       : in    std_logic;
    Write_en      : in    std_logic;
    ReadData   : out   std_logic_vector (7 downto 0);
    WriteData  : in    std_logic_vector (7 downto 0);
    -- Parallel Port external interface
    ParPort    : inout std_logic_vector (7 downto 0)
    );
end ParallelPort;

architecture rtl of ParallelPort is

  signal iRegDir  : std_logic_vector (7 downto 0);
  signal iRegPort : std_logic_vector (7 downto 0);
  signal iRegPin  : std_logic_vector (7 downto 0);
begin
  pPort : process (iRegDir, iRegPort)
  begin
    for i in 0 to 7 loop
      if iRegDir(i) = '1' then
        ParPort(i) <= iRegPort(i);
      else
        ParPort(i) <= 'Z';
      end if;
    end loop;
  end process pPort;  -- Parallel Port Input value

  iRegPin <= ParPort;

  pRegWr : process (Clk, nReset)
  begin
    if nReset = '0' then
      iRegDir <= (others => '0');               -- Input by default
    elsif rising_edge(Clk) then
      if ChipSelect = '1' and Write_en = '1' then  -- Write cycle
        case Address(2 downto 0) is
          when "000"  => iRegDir  <= WriteData;
          when "010"  => iRegPort <= WriteData;
          when "011"  => iRegPort <= iRegPort or WriteData;
          when "100"  => iRegPort <= iRegPort and not WriteData;
          when others => null;
        end case;
      end if;
    end if;
  end process pRegWr;

  pRegRd : process (Clk, nReset)
  begin
if rising_edge(Clk) then
	 ReadData <= (others => '0');
      if (ChipSelect = '1' and Read_en = '1') then  -- Read cycle
        case Address(2 downto 0) is
          when "000"  => ReadData <= iRegDir;
          when "001"  => ReadData <= iRegPin;
          when "010"  => ReadData <= iRegPort;
          when others => null;
        end case;
      end if;
    end if;
  end process pRegRd;
end architecture rtl;
