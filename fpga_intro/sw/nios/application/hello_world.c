/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */

#include <stdio.h>
#include <inttypes.h>
#include "system.h"
#include "io.h"
//#include "altera_avalon_pio_regs.h"
#define PARALLELPORT_0_BASE 0x41000

int main()
{
  //printf("Hello from Nios II!\n");
  int c = 0x1;
  int j = 0x1;
  // setting all as output
  IOWR_8DIRECT(PARALLELPORT_0_BASE,0x0, 0xFF);
  //IOWR_8DIRECT(PARALLELPORT_0_BASE,0x2, 0xFF);
  //while (1) {}

  for ( ; ; ) {
	  //printf("i=%d\n",c);
	  IOWR_8DIRECT(PARALLELPORT_0_BASE,0x2, c);
	  if (c == 0x80) {
		  c = 0x1;
	  }	  else	  {
		  c = c<<1;
	  }
	  for (j=0;j<1000000;j++) {}
 }
  return 0;
}
