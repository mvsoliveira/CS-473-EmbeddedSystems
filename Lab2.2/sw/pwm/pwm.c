//////////////////////////////////////////////////////////////////////////////
// Title      : PWM modulator software configuration
// Project    : CS/473 Embedded Systems
//////////////////////////////////////////////////////////////////////////////
// File       : pwm.c
// Author     : Marcos Oliveira
// Company    : CERN
// Created    : 2016/11/17
// Last update: 2016/11/26
// Platform   : Nios II 16.0 Software Build Tools for Eclipse
// Standard   : C
//////////////////////////////////////////////////////////////////////////////
// Description: Implements PWM software configuration 
//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2016 CERN
//////////////////////////////////////////////////////////////////////////////
// Revisions  :
// Date        Version  Author  Description
// 2016/11/17  1.0      msilvaol        Created
//////////////////////////////////////////////////////////////////////////////

#include "system.h"
#include "io.h"
int main()
{
  volatile int i;
  volatile int j;
  // polarity 0 and dividing 50 MHz by 2^9
  int poldiv  = 0x09;
  // period = 200*(1/(50MHz/2^9)) = 2.048 ms
  int period  = 200;
  // dutty cycle = 0% when pol = 1
  int setmin  = 0;
  // dutty cycle = 100% when pol = 1
  int setmax  = period;
  // refresh time
  int time = 5000;
  // writing period value register
  IOWR_32DIRECT(PWM_0_BASE,0x4, period);
  while( 1 )
  {
	  // inverting polarity
	  poldiv ^= 0x10;
	  // writing polarity and divider
	  IOWR_32DIRECT(PWM_0_BASE,0x0, poldiv);
	  // looping duty-cycle values from setmin and setmax
	  for (i=setmin;i<setmax;i++) {
		  IOWR_32DIRECT(PWM_0_BASE,0x8, i);
		  for (j=0;j<time;j++) {}
	  }
  }
  return 0;
}