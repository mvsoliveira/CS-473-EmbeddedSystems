------------------------------------------------------------------------------------------
-- Title      : Testbench for design "pwm"
-- Project    : CS/473 Embedded Systems

------------------------------------------------------------------------------------------
-- File       : pwm_tb.vhd
-- Author     : Marcos Oliveira
-- Company    : CERN
-- Created    : 2016-11-17
-- Last update: 2016-11-18
-- Platform   : Symplify Premier G-2012.09 and Mentor Modelsim SE-64 10.1c
-- Standard   : VHDL'93/02
------------------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------------------
-- Copyright (c) 2016 CERN
------------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-11-17  1.0      msilvaol	Created
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

------------------------------------------------------------------------------------------

entity pwm_tb is

end entity pwm_tb;

------------------------------------------------------------------------------------------

architecture tb of pwm_tb is

  -- component generics
  constant n_div_outs : natural := 16;
  constant log2_n_div_outs : natural := 4;

  -- component ports
  signal Clk        : std_logic;
  signal nReset     : std_logic;
  signal Address    : std_logic_vector (1 downto 0);
  signal ChipSelect : std_logic;
  signal Read       : std_logic;
  signal Write      : std_logic;
  signal ReadData   : std_logic_vector (31 downto 0);
  signal WriteData  : std_logic_vector (31 downto 0);
  signal pwm_out    : std_logic;

  constant gbl_clk_period : time    := 20 ns;


begin  -- architecture tb

  -- component instantiation
  DUT: entity work.pwm
    generic map (
      n_div_outs => n_div_outs,
      log2_n_div_outs => log2_n_div_outs)
    port map (
      Clk        => Clk,
      nReset     => nReset,
      Address    => Address,
      ChipSelect => ChipSelect,
      Read       => Read,
      Write      => Write,
      ReadData   => ReadData,
      WriteData  => WriteData,
      pwm_out    => pwm_out);

  
  -- clock generation
    gbl_clk : process
  begin
    Clk <= '0';
    wait for gbl_clk_period/2;
    Clk <= '1';
    wait for gbl_clk_period/2;
  end process;
  
  

  -- waveform generation
  WaveGen_Proc: process
  begin
    -- insert signal assignments here
	 nReset <= '0';
	 Address <= (others => '0');
	 ChipSelect <= '0';
	 Write <= '0';
	 Read <= '0';
	 WriteData <= (others => '0');
	 wait for 20 ns;
	 nReset <= '1';
	 Address <= "00";
	 ChipSelect <= '1';
	 Write <= '1';
	 WriteData <= x"00000002";
	 wait for 20 ns;
	 Address <= "01";
	 WriteData <= x"00000003";
	 wait for 20 ns;
	 Address <= "10";
	 WriteData <= x"00000000";
         wait for 2000 ns;
         WriteData <= x"00000001";
         wait for 2000 ns;
         WriteData <= x"00000002";
         wait for 2000 ns;
         WriteData <= x"00000003";
         wait for 2000 ns;
         WriteData <= x"00000004";
         wait for 2000 ns;
         Write <= '0';
	 Read <= '1';
         Address <= "00";
	 wait for 20 ns;
	 Address <= "10";
	 Address <= "01";
	 wait for 20 ns;
	 Address <= "10";
         wait for 20 ns;
         ChipSelect <= '0';
	 Read <= '0';
    wait;
  end process WaveGen_Proc;

  
end architecture tb;

------------------------------------------------------------------------------------------

configuration pwm_tb_tb_cfg of pwm_tb is
  for tb
  end for;
end pwm_tb_tb_cfg;

------------------------------------------------------------------------------------------
