onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pwm_tb/Clk
add wave -noupdate /pwm_tb/nReset
add wave -noupdate /pwm_tb/Address
add wave -noupdate /pwm_tb/ChipSelect
add wave -noupdate -radix hexadecimal /pwm_tb/Read
add wave -noupdate -radix hexadecimal /pwm_tb/Write
add wave -noupdate -radix hexadecimal /pwm_tb/ReadData
add wave -noupdate -radix hexadecimal /pwm_tb/WriteData
add wave -noupdate -expand /pwm_tb/DUT/enable
add wave -noupdate /pwm_tb/DUT/iPolReg
add wave -noupdate -radix hexadecimal /pwm_tb/DUT/iDivReg
add wave -noupdate -radix hexadecimal /pwm_tb/DUT/iPerReg
add wave -noupdate -radix hexadecimal /pwm_tb/DUT/iSetReg
add wave -noupdate /pwm_tb/DUT/pwm_ena
add wave -noupdate -radix hexadecimal /pwm_tb/DUT/pwm_cnt
add wave -noupdate /pwm_tb/pwm_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {99752884 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 149
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 4300000
configure wave -griddelta 2
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {11550 ns}
