----------------------------------------------------------------------------------------------------------------------
-- Title      : PWM modulator
-- Project    : CS-473 Embedded Systems

----------------------------------------------------------------------------------------------------------------------
-- File       : pwm.vhd
-- Author     : Marcos Oliveira
-- Company    : CERN
-- Created    : 2016-11-17
-- Last update: 2016-11-26
-- Platform   : Symplify Premier G-2012.09 and Mentor Modelsim SE-64 10.1c
-- Standard   : VHDL'93/02
----------------------------------------------------------------------------------------------------------------------
-- Description: Implements PWM modulator with programmable clock divider
-------------------------------------------------------------------------------
-- Copyright (c) 2016 CERN
----------------------------------------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-11-17  1.0      msilvaol        Created
----------------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pwm is
  generic (
    n_div_outs      : natural := 16;
    log2_n_div_outs : natural := 4);
  port (
    -- Avalon interfaces signals
    Clk        : in  std_logic;
    nReset     : in  std_logic;
    Address    : in  std_logic_vector (1 downto 0);
    ChipSelect : in  std_logic;
    Read       : in  std_logic;
    Write      : in  std_logic;
    ReadData   : out std_logic_vector (31 downto 0);
    WriteData  : in  std_logic_vector (31 downto 0);
    -- Parallel Port external interface
    pwm_out    : out std_logic
    );
end pwm;

architecture rtl of pwm is

  -- Programmable clock divider
  signal counter_v : std_logic_vector(n_div_outs-1 downto 1);
  signal counter_d : std_logic_vector(n_div_outs-1 downto 1);
  signal enable    : std_logic_vector(n_div_outs-1 downto 0);
  -- Avalon registers
  signal iPolReg   : std_logic;
  signal iDivReg   : unsigned(log2_n_div_outs-1 downto 0);
  signal iPerReg   : unsigned(31 downto 0);
  signal iSetReg   : unsigned(31 downto 0);
  -- pwm
  signal pwm_ena   : std_logic;
  signal pwm_cnt   : unsigned(31 downto 0);
begin  -- architecture rtl

  ----------------------------------------------------------------------------------------------------------------------
  -- Programmable clock divider
  ----------------------------------------------------------------------------------------------------------------------

  divider : process (Clk, nReset)
    constant cntmax  : natural := 2**(n_div_outs-1)-1;
    variable counter : integer range 0 to cntmax;
  begin
    if nReset = '0' then
      counter   := 0;
      counter_v <= (others => '0');
      counter_d <= (others => '0');
      enable    <= (others => '0');
    elsif rising_edge(Clk) then
      if counter < cntmax then
        counter := counter + 1;
      else
        counter := 0;
      end if;
      counter_v <= std_logic_vector(to_unsigned(counter, counter_v'length));
      counter_d <= counter_v;
      enable(0) <= '1';
      for i in counter_v'range loop
        enable(i) <= counter_v(i) and not counter_d(i);
      end loop;
    end if;
  end process divider;

  ----------------------------------------------------------------------------------------------------------------------
  -- Avalon registers
  ----------------------------------------------------------------------------------------------------------------------

  pRegWr : process (Clk, nReset)
  begin
    if nReset = '0' then
      iPolReg <= '0';
      iDivReg <= (others => '0');
      iPerReg <= (others => '0');
      iSetReg <= (others => '0');
    elsif rising_edge(Clk) then
      if ChipSelect = '1' and Write = '1' then  -- Write cycle
        case Address(1 downto 0) is
          when "00" =>
            iPolReg <= WriteData(log2_n_div_outs);
            iDivReg <= unsigned(WriteData(log2_n_div_outs-1 downto 0));
          when "01" =>
            iPerReg <= unsigned(WriteData);
          when "10" =>
            iSetReg <= unsigned(WriteData);
          when others => null;
        end case;
      end if;
    end if;
  end process pRegWr;

  pRegRd : process (Clk, nReset)
  begin
    if rising_edge(Clk) then
      ReadData <= (others => '0');
      if (ChipSelect = '1' and Read = '1') then  -- Read cycle
        case Address(1 downto 0) is
          when "00" =>
            ReadData(log2_n_div_outs)            <= iPolReg;
            ReadData(log2_n_div_outs-1 downto 0) <= std_logic_vector(iDivReg);
          when "01" =>
            ReadData <= std_logic_vector(iPerReg);
          when "10" =>
            ReadData <= std_logic_vector(iSetReg);
          when others => null;
        end case;
      end if;
    end if;
  end process pRegRd;

  ----------------------------------------------------------------------------------------------------------------------
  -- PWM
  ----------------------------------------------------------------------------------------------------------------------

  pwm_p : process (Clk, nReset)
    variable pwm_var : std_logic;
  begin
    if nReset = '0' then
      pwm_ena <= '0';
      pwm_cnt <= (others => '0');
      pwm_out <= '0';
    elsif rising_edge(Clk) then
      -- PWM counter
      pwm_ena <= enable(to_integer(iDivReg));
      if pwm_ena = '1' then
        if pwm_cnt < iPerReg then
          pwm_cnt <= pwm_cnt + 1;
        else
          pwm_cnt <= (others => '0');
        end if;
      end if;
      -- PWM output
      if pwm_cnt >= iSetReg then
        pwm_var := '1';
      else
        pwm_var := '0';
      end if;
      if iPolReg = '0' then
        pwm_out <= pwm_var;
      else
        pwm_out <= not pwm_var;
      end if;
    end if;
  end process pwm_p;
  
end architecture rtl;
