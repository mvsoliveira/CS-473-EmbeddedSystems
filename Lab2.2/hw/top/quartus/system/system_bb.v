
module system (
	clk_clk,
	pwm_0_conduit_end_export,
	reset_reset_n);	

	input		clk_clk;
	output	[7:0]	pwm_0_conduit_end_export;
	input		reset_reset_n;
endmodule
