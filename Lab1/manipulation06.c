#include <msp430g2553.h>
int time_cnt (int time_ms) {return (time_ms << 4) - (3*time_ms>>3);}
int set_adc10(){
	// select clock SMCLK, divider = 0, set ADC mode to single channel single-conversion, and input A1, CAPD(1) is already 0
	ADC10CTL1 = ADC10SSEL_3 | ADC10DIV_0 | INCH_1 | CONSEQ_0;
	// select p1.3 as output
	P1DIR |= BIT3;
	// set p1.3 out to gnd
	P1OUT = 0;
	// setting reference voltage Vcc/Vss, REFON=1, tsample = 64 cycles, enabling interrupts
	ADC10CTL0 = SREF_0 | BIT5 | ADC10SHT_3 | BIT3;
	// ADC10 on
	ADC10CTL0 |= BIT4;
	// ADC10 enable conversion and taking first sample
	ADC10CTL0 |= BIT1 | BIT0;
	// returning OK
	return 0;
}
int set_timer(int period)
{
	TA0CCTL0 = CCIE; // enabling interrupts
	TA0CCR0 = time_cnt(period); // calculating time counter value
	TA0CTL = TASSEL_2 | ID_3 | MC_1; // firing counter in up mode
}
#pragma vector=TIMER0_A0_VECTOR
__interrupt void sample_adc (void)
{
	P1OUT ^= 0x01;
	// has to remember to toogle ENC when using timer signals to trigger ADC
	// taking new sample
		ADC10CTL0 |= BIT0;
	//TA0CTL &= (~TAIFG);
}

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= 0x01;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL = CALDCO_1MHZ;
	BCSCTL2 = DIVS_3; // dividing smclk by 8
	set_adc10(); // setting ADC

	#define PERIOD_MS 50
	set_timer(PERIOD_MS);
	_BIS_SR(CPUOFF + GIE); // enter in low power mode and wakingup when an interrupt occurs

	return 0;
}
