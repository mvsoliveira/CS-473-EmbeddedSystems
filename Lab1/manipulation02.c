
#include <msp430g2553.h>

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= 0x3F;					// Set P1.0 to output direction
	DCOCTL = 0;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL = CALDCO_1MHZ;
	ID	00 - ID_0	Timer A clock input divider 1



	/* LED blinking frequency */
	#define LED_BLINK_FREQ_HZ 1

	/* Number of cycles to delay based on 1MHz MCLK */
	#define LED_DELAY_CYCLES (1000000 / (2 * LED_BLINK_FREQ_HZ))
	P1OUT ^= 0x01;
	while (1) {
	    /* Wait for LED_DELAY_CYCLES cycles */
	    __delay_cycles(LED_DELAY_CYCLES);

	    if (P1OUT == 0x20)
	    {
	    	P1OUT = 0x01;
	    }
	    else
	    {
	    	P1OUT = P1OUT << 1;
	    }
	}
	
	return 0;
}
