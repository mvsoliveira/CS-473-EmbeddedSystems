#include <msp430g2553.h>
int time_cnt (int time_ms) {return (time_ms << 4) - (3*time_ms>>3);}
int pwm_timer(int period, int set_tm)
{
	TA0CCR0 = time_cnt(period); // calculating period counter value
	TA0CCR1 = time_cnt(set_tm); // calculating set_time counter value
	TA0CCTL1 = OUTMOD_3; // setting output mode set/reset
	TA0CTL = TASSEL_2 | ID_3 | MC_1; // firing counter in up mode
}

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= BIT2;					// Set P1.0 to output direction
	P1SEL |= BIT2;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL = CALDCO_1MHZ;
	BCSCTL2 = DIVS_3; // dividing smclk by 8
	TA0CTL = TASSEL_2 | ID_3 | MC_0; // selecting smclk and dividing again by 8, timer halted

	/* LED blinking frequency */
	#define PERIOD_MS 10
    #define SET_TIME_MS 2
	pwm_timer(PERIOD_MS,SET_TIME_MS);

	
	return 0;
}
