#include <msp430g2553.h>
int time_cnt (int time_ms) {return (time_ms << 4) - (3*time_ms>>3);}
int wait_ms(int period)
{
	TA0CCR0 = time_cnt(period); // calculating time counter value
	TA0CTL = TASSEL_2 | ID_3 | MC_1; // firing counter in up mode
	while (1) {
		if (TA0CCTL0 & CCIFG)
		{
			TA0CTL = TASSEL_2 | ID_3 | MC_0; // halting timer for saving power
			TA0CCTL0 = CCIE; // clearing CCIFG flag
			return 0;
		}
	}
}

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= 0x01;					// Set P1.0 to output direction
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL = CALDCO_1MHZ;
	BCSCTL2 = DIVS_3; // dividing smclk by 8
	TA0CTL = TASSEL_2 | ID_3 | MC_0; // selecting smclk and dividing again by 8, timer halted

	#define LED_BLINK_DELAY_MS 500

	while (1) {
			wait_ms(LED_BLINK_DELAY_MS);
			P1OUT ^= 0x01;
	}
	
	return 0;
}
