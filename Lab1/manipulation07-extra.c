#include < msp430g2553.h >
int time_cnt(int adc_value) {return (adc_value << 2) - (7 * adc_value >> 3);}
int set_adc10() {
  // select clock SMCLK, divider = 8, set ADC mode to single channel single-conversion, and input A1, CAPD(1) is already 0
  ADC10CTL1 = ADC10SSEL_3 | ADC10DIV_7 | INCH_1 | CONSEQ_0 | SHS_1;
  // select p1.3 as output
  P1DIR |= BIT3;
  // set p1.3 out to gnd
  P1OUT = 0;
  // setting reference voltage Vcc/Vss, REFON=1, tsample = 64 cycles, enabling interrupts
  ADC10CTL0 = SREF_0 | BIT5 | ADC10SHT_3 | BIT3;
  // ADC10 on
  ADC10CTL0 |= BIT4;
  // ADC10 enable conversion
  ADC10CTL0 |= BIT1;
  // returning OK
  return 0;
}
int set_timer() {
  TA0CCR0 = 50000; // 50ms with 8 MHz, smclk divider=0,timerdivider=8
  TA0CCTL1 = OUTMOD_3; // for generating adc refresh pulse
  TA0CCR1 = 50000; // 50ms with 8 MHz, smclk divider=0,timerdivider=8
  TA0CTL = TASSEL_2 | ID_3 | MC_1; // firing counter in up mode, div=8
}
int pwm_timer() {
  TA1CCR0 = 16000; // fixed pwm period counter value
  TA1CCR1 = 6400; // fixed minimum set counter value
  TA1CCTL1 = OUTMOD_3; // setting output mode set/reset
  TA1CTL = TASSEL_2 | ID_0 | MC_1; // firing counter in up mode, div=1
}
#pragma vector = ADC10_VECTOR
__interrupt void read_adc(void) {
  ADC10CTL0 |= ENC; // asserting ENC at every new sample. Mandatory with triggers from timers units 
  P1OUT ^= 0x01; // toggling led for adc value refresh monitoring
  TA1CCR1 = 6400 + time_cnt(ADC10MEM); // adding offset to set time based on adc value with full dynamic range
}
int main(void) {
  WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer
  P1DIR |= BIT0 | BIT6; // led and p1.6 monitors adc value refresh 50ms period pulse
  P1SEL |= BIT6; // p1.6 monitors adc value refresh 50ms period pulse
  P2DIR |= BIT2; // pwm output from TimerA1 Output unit 1
  P2SEL |= BIT2; // pwm output from TimerA1 Output unit 1
  BCSCTL1 = CALBC1_8MHZ; // select 8MHz clock to have a low timer step duration in order to use every ADC output code
  DCOCTL = CALDCO_8MHZ; // timer step = 1/8MHz = 125 ns < (pulse width variation/ADC codes) < 0.4 ms / 1024 < 390 ns
  pwm_timer(); // setting pwm timer
  set_adc10(); // setting ADC
  set_timer(); // setting ADC refresh timer
  _BIS_SR(CPUOFF + GIE); // enter in low power mode and waking up when an interrupt occurs
  return 0;
}