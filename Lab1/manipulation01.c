//***************************************************************************************
//  MSP430 Blink the LED Demo - Software Toggle P1.0
//
//  Description; Toggle P1.0 by xor'ing P1.0 inside of a software loop.
//  ACLK = n/a, MCLK = SMCLK = default DCO
//
//                MSP430x5xx
//             -----------------
//         /|\|              XIN|-
//          | |                 |
//          --|RST          XOUT|-
//            |                 |
//            |             P1.0|-->LED
//
//  J. Stevenson
//  Texas Instruments, Inc
//  July 2011
//  Built with Code Composer Studio v5
//***************************************************************************************

#include <msp430g2553.h>

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= 0x01;					// Set P1.0 to output direction
	DCOCTL = 0;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL = CALDCO_1MHZ;


	/* LED blinking frequency */
	#define LED_BLINK_FREQ_HZ 1

	/* Number of cycles to delay based on 1MHz MCLK */
	#define LED_DELAY_CYCLES (1000000 / (2 * LED_BLINK_FREQ_HZ))

	while (1) {
	    /* Wait for LED_DELAY_CYCLES cycles */
	    __delay_cycles(LED_DELAY_CYCLES);

	    /* Toggle P1.0 output */
	    P1OUT ^= 0x01;
	}
	
	return 0;
}
