#include <msp430g2553.h>
int time_cnt (int time_ms) {return (time_ms << 4) - (3*time_ms>>3);}
int set_timer(int period)
{
	TA0CCTL0 = CCIE; // enabling interrupts
	TA0CCR0 = time_cnt(period); // calculating time counter value
	TA0CTL = TASSEL_2 | ID_3 | MC_1; // firing counter in up mode
}
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
	P1OUT ^= 0x01;
	//TA0CTL &= (~TAIFG);
}

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= 0x01;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL = CALDCO_1MHZ;
	BCSCTL2 = DIVS_3; // dividing smclk by 8

	#define PERIOD_MS 50
	set_timer(PERIOD_MS);
	_BIS_SR(CPUOFF + GIE); // enter in low power mode and wakingup when an interrupt occurs
	return 0;
}
