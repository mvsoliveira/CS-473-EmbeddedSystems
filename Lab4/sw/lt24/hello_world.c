#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "sys/alt_irq.h" // the irq functions#include "io.h"
#include "system.h"

#define ONE_MB (1024 * 1024)
#define FIFO_LENGTH (256)
#define FRAME_LENGTH (320*240/2)
#define BURST_LENGTH (32)
#define N_BUFFERS (24)
#define FRAME_INTERVAL_MS (100)

uint32_t pointers[N_BUFFERS];
uint32_t p = N_BUFFERS - 1;
int32_t frame_int = FRAME_INTERVAL_MS * 1000;

int memtst(uint32_t testlength) {
	printf("Starting Memory Test... \n");
	if (testlength < 0) {
		testlength = HPS_0_BRIDGES_SPAN;
	}
	uint32_t megabyte_count = 0;

	for (uint32_t i = 0; i < testlength; i += sizeof(uint32_t)) {

		// Print progress through 256 MB memory available through address span expander
		if ((i % ONE_MB) == 0) {
			printf("megabyte_count = %" PRIu32 "\n", megabyte_count);
			megabyte_count++;
		}

		uint32_t addr = HPS_0_BRIDGES_BASE + i;

		// Write through address span expander
		uint32_t writedata = i;
		IOWR_32DIRECT(addr, 0, writedata);

		// Read through address span expander
		uint32_t readdata = IORD_32DIRECT(addr, 0);

		// Check if read data is equal to written data
		assert(writedata == readdata);
	}

	return EXIT_SUCCESS;
}

void set_lcd_on(uint32_t value) {
	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x8, value);
	if (IORD_32DIRECT(LCD_CONTROLLER_0_BASE, 0x8) == value) {
		printf("LCD_ON set successfully!\n");
	} else {
		printf("LCD_ON did not set successfully!\n");
	}
}

void set_lcd_reset(uint32_t value) {
	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0xC, value);
}

void Delay_Ms(uint32_t value) {
	usleep(value * 1000);
}

void Delay_Us(uint32_t value) {
	usleep(value);
}

void LCD_WR_REG(uint32_t value) {
	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x0, value);
}
void LCD_WR_DATA(uint32_t value) {
	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x4, value);
}

void LCD_RD_DATA() {
	IORD_32DIRECT(LCD_CONTROLLER_0_BASE, 0x4);
}

uint32_t get_lcd_data() {
	return IORD_32DIRECT(LCD_CONTROLLER_0_BASE, 0x10);
}

uint32_t read_lcd_data() {
	LCD_RD_DATA();
	return get_lcd_data();
}
uint32_t read_madctl() {
	LCD_WR_REG(0x0B);
	LCD_RD_DATA();
	LCD_RD_DATA();
	return get_lcd_data();
}

uint32_t read_bytes(uint32_t n) {
	volatile uint32_t data;
	volatile uint32_t i = 0;
	for (uint32_t a = 0; a < n; a++) {
		data = read_lcd_data();
		i = i + (data << a * 8);
	}
	return i;
}

void read_RDDIDIF() {
	LCD_WR_REG(0x04);
	LCD_RD_DATA(); // dummy
	printf("Read Display Identification Information = 0x%06x.\n",
			read_bytes(3));
}

void read_RDDST() {
	LCD_WR_REG(0x09);
	LCD_RD_DATA(); // dummy
	printf("Read Display Status = 0x%08x.\n", read_bytes(4));
}

void read_RDDPM() {
	LCD_WR_REG(0x0A);
	LCD_RD_DATA(); // dummy
	printf("Read Display Power Mode = 0x%02x.\n", read_bytes(1));
}

void read_RDDMADCTL() {
	LCD_WR_REG(0x0B);
	LCD_RD_DATA(); // dummy
	printf("Read Display MADCTL = 0x%02x.\n", read_bytes(1));
}

void read_RDDCOLMOD() {
	LCD_WR_REG(0x0C);
	LCD_RD_DATA(); // dummy
	printf("Read Display Pixel Format = 0x%02x.\n", read_bytes(1));
}

void read_RDDIM() {
	LCD_WR_REG(0x0D);
	LCD_RD_DATA(); // dummy
	printf("Read Display Image Mode = 0x%02x.\n", read_bytes(1));
}

void read_RDDSM() {
	LCD_WR_REG(0x0E);
	LCD_RD_DATA(); // dummy
	printf("Read Display Signal Mode = 0x%02x.\n", read_bytes(1));
}

void read_RDDSDR() {
	LCD_WR_REG(0x0F);
	LCD_RD_DATA(); // dummy
	printf("Read Display Self-Diagnostic Result = 0x%02x.\n", read_bytes(1));
}

void read_frame_mem() {
	LCD_WR_REG(0x2E);
	LCD_RD_DATA();
	printf("Frame Memory = 0x%08x.\n", read_bytes(1));
}

void report_lcd_info() {
	read_RDDIDIF();
	read_RDDST();
	read_RDDPM();
	read_RDDMADCTL();
	read_RDDCOLMOD();
	read_RDDIM();
	read_RDDSM();
	read_RDDSDR();
}

void lcd_init() {
	set_lcd_reset(0x1);
	Delay_Ms(1);
	set_lcd_reset(0x0);
	Delay_Ms(10); // Delay 10ms // This delay time is necessary
	set_lcd_reset(0x1);
	Delay_Ms(120); // Delay 120 ms
	LCD_WR_REG(0x0011); //Exit Sleep
	LCD_WR_REG(0x00CF); // Power Control B
	LCD_WR_DATA(0x0000); // Always0x00
	LCD_WR_DATA(0x0081); //
	LCD_WR_DATA(0X00c0);
	LCD_WR_REG(0x00ED); // Power on sequencecontrol
	LCD_WR_DATA(0x0064); // Soft Start Keep1 frame
	LCD_WR_DATA(0x0003); //
	LCD_WR_DATA(0X0012);
	LCD_WR_DATA(0X0081);
	LCD_WR_REG(0x00E8); // Driver timing control A
	LCD_WR_DATA(0x0085);
	LCD_WR_DATA(0x0001);
	LCD_WR_DATA(0x00798);
	LCD_WR_REG(0x00CB); // Power control A
	LCD_WR_DATA(0x0039);
	LCD_WR_DATA(0x002C);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0034);
	LCD_WR_DATA(0x0002);
	LCD_WR_REG(0x00F7); // Pumpratio control
	LCD_WR_DATA(0x0020);
	LCD_WR_REG(0x00EA); // Driver timing control B
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0000);
	LCD_WR_REG(0x00B1); // Frame Control (In Normal Mode)
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x001b);
	LCD_WR_REG(0x00B6); // Display FunctionControl
	LCD_WR_DATA(0x000A);
	LCD_WR_DATA(0x00A2);
	LCD_WR_REG(0x00C0); //Power control 1
	LCD_WR_DATA(0x0005); //VRH[5:0]
	LCD_WR_REG(0x00C1); //Power control 2
	LCD_WR_DATA(0x0011); //SAP[2:0];BT[3:0]
	LCD_WR_REG(0x00C5); //VCM control 1
	LCD_WR_DATA(0x0045); //3F
	LCD_WR_DATA(0x0045); //3C
	LCD_WR_REG(0x00C7); //VCM control 2
	LCD_WR_DATA(0X00a2);
	//LCD_WR_REG(0x0036); // Memory Access Control
	//LCD_WR_DATA(0x0008); // BGR order
	LCD_WR_REG(0x0036);
	int32_t MH = 1 << 2;
	int32_t BGR = 1 << 3;
	int32_t ML = 1 << 4;
	int32_t MV = 1 << 5;
	int32_t MX = 1 << 6;
	int32_t MY = 1 << 7;
	LCD_WR_DATA(MH + BGR + ML + MV + MY);
	LCD_WR_REG(0x00F2); // Enable3G
	LCD_WR_DATA(0x0000); // 3Gamma FunctionDisable
	LCD_WR_REG(0x0026); // Gamma Set
	LCD_WR_DATA(0x0001); // Gamma curveselected
	LCD_WR_REG(0x00E0); // Positive Gamma Correction, Set Gamma
	LCD_WR_DATA(0x000F);
	LCD_WR_DATA(0x0026);
	LCD_WR_DATA(0x0024);
	LCD_WR_DATA(0x000b);
	LCD_WR_DATA(0x000E);
	LCD_WR_DATA(0x0008);
	LCD_WR_DATA(0x004b);
	LCD_WR_DATA(0X00a8);
	LCD_WR_DATA(0x003b);
	LCD_WR_DATA(0x000a);
	LCD_WR_DATA(0x0014);
	LCD_WR_DATA(0x0006);
	LCD_WR_DATA(0x0010);
	LCD_WR_DATA(0x0009);
	LCD_WR_DATA(0x0000);
	LCD_WR_REG(0X00E1); //NegativeGamma Correction, Set Gamma
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x001c);
	LCD_WR_DATA(0x0020);
	LCD_WR_DATA(0x0004);
	LCD_WR_DATA(0x0010);
	LCD_WR_DATA(0x0008);
	LCD_WR_DATA(0x0034);
	LCD_WR_DATA(0x0047);
	LCD_WR_DATA(0x0044);
	LCD_WR_DATA(0x0005);
	LCD_WR_DATA(0x000b);
	LCD_WR_DATA(0x0009);
	LCD_WR_DATA(0x002f);
	LCD_WR_DATA(0x0036);
	LCD_WR_DATA(0x000f);
	LCD_WR_REG(0x002A); // ColumnAddressSet
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x00ef);
	LCD_WR_REG(0x002B); // Page AddressSet
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0001);
	LCD_WR_DATA(0x003f);
	LCD_WR_REG(0x003A); // COLMOD: Pixel Format Set
	LCD_WR_DATA(0x0055);
	LCD_WR_REG(0x00f6); // Interface Control
	LCD_WR_DATA(0x0001);
	LCD_WR_DATA(0x0030);
	LCD_WR_DATA(0x0000);
	LCD_WR_REG(0x0029); //display on
	LCD_WR_REG(0x002c); // Start Frame Memory write
	printf("Initialisation finished successfully.\n");
}

void fill_frame(uint32_t value) {
	LCD_WR_REG(0x002c); // Start Frame Memory write
	for (uint32_t a = 0; a < 240 * 320; a++) {
		LCD_WR_DATA(value);
	}
}

void RAM_Init_Pic(uint32_t start, char* filename) {

	FILE *foutput = NULL;
	foutput = fopen(filename, "rb");
	if (!foutput) {
		printf("Error: could not open \"%s\" for reading\n", filename);
	}

	printf("File open now writing it to memory\n");
	for (uint32_t i = start; i < 160 * 240 * sizeof(uint32_t) + start; i +=
			sizeof(uint32_t)) {

		unsigned char buffer[4];
		fread(buffer, sizeof(buffer), 1, foutput);
		uint32_t addr = HPS_0_BRIDGES_BASE + i;
		uint32_t writedata = (buffer[0] << 24) + (buffer[1] << 16)
				+ (buffer[2] << 8) + buffer[3];
		IOWR_32DIRECT(addr, 0, writedata);
	}

}

void RAM_Init_Pic_Cnt(uint32_t start) {

	for (uint32_t i = start; i < 160 * 240 * sizeof(uint32_t) + start; i +=
			sizeof(uint32_t)) {

		uint32_t addr = HPS_0_BRIDGES_BASE + i;
		uint32_t writedata = i;
		IOWR_32DIRECT(addr, 0, writedata);
	}

}

void LCD_Init_Pic(char* filename) {

	FILE *foutput = NULL;
	foutput = fopen(filename, "rb");
	if (!foutput) {
		printf("Error: could not open \"%s\" for reading\n", filename);
	}

	printf("File open now writing it to LCD\n");
	LCD_WR_REG(0x002c); // Start Frame Memory write
	for (uint32_t i = 0; i < 160 * 240; i++) {

		unsigned char buffer[4];
		fread(buffer, sizeof(buffer), 1, foutput);
		LCD_WR_DATA((buffer[2] << 8) + buffer[3]);
		LCD_WR_DATA((buffer[0] << 8) + buffer[1]);
	}

}

void LCD_Init() {
	printf("INIT STARTED \n");
	set_lcd_reset(0x1);
	Delay_Ms(1);
	set_lcd_reset(0x0);
	Delay_Ms(10); // Delay 10ms // This delay time is necessary
	set_lcd_reset(0x1);
	Delay_Ms(120); // Delay 120 ms

	LCD_WR_REG(0x0011); //Exit Sleep
	LCD_WR_REG(0x00CF);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0081);
	LCD_WR_DATA(0X00c0);

	LCD_WR_REG(0x00ED);
	LCD_WR_DATA(0x0064);
	LCD_WR_DATA(0x0003);
	LCD_WR_DATA(0X0012);
	LCD_WR_DATA(0X0081);

	LCD_WR_REG(0x00E8);
	LCD_WR_DATA(0x0085);
	LCD_WR_DATA(0x0001);
	LCD_WR_DATA(0x00798);

	LCD_WR_REG(0x00CB);
	LCD_WR_DATA(0x0039);
	LCD_WR_DATA(0x002C);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0034);
	LCD_WR_DATA(0x0002);

	LCD_WR_REG(0x00F7);
	LCD_WR_DATA(0x0020);

	LCD_WR_REG(0x00EA);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0000);

	LCD_WR_REG(0x00B1);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x001b);

	LCD_WR_REG(0x00B6);
	LCD_WR_DATA(0x000A);
	LCD_WR_DATA(0x00A2);

	LCD_WR_REG(0x00C0);    //Power control
	LCD_WR_DATA(0x0005);   //VRH[5:0]

	LCD_WR_REG(0x00C1);    //Power control
	LCD_WR_DATA(0x0011);   //SAP[2:0];BT[3:0]

	LCD_WR_REG(0x00C5);    //VCM control
	LCD_WR_DATA(0x0045);       //3F
	LCD_WR_DATA(0x0045);       //3C

	LCD_WR_REG(0x00C7);    //VCM control2
	LCD_WR_DATA(0X00a2);

	LCD_WR_REG(0x0036);    // Memory Access Control will be overwritten below
	LCD_WR_DATA(0x0008);    //48

	LCD_WR_REG(0x00F2);    // 3Gamma Function Disable
	LCD_WR_DATA(0x0000);

	LCD_WR_REG(0x0026);    //Gamma curve selected
	LCD_WR_DATA(0x0001);

	LCD_WR_REG(0x00E0);    //Set Gamma
	LCD_WR_DATA(0x000F);
	LCD_WR_DATA(0x0026);
	LCD_WR_DATA(0x0024);
	LCD_WR_DATA(0x000b);
	LCD_WR_DATA(0x000E);
	LCD_WR_DATA(0x0008);
	LCD_WR_DATA(0x004b);
	LCD_WR_DATA(0X00a8);
	LCD_WR_DATA(0x003b);
	LCD_WR_DATA(0x000a);
	LCD_WR_DATA(0x0014);
	LCD_WR_DATA(0x0006);
	LCD_WR_DATA(0x0010);
	LCD_WR_DATA(0x0009);
	LCD_WR_DATA(0x0000);

	LCD_WR_REG(0X00E1);    //Set Gamma
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x001c);
	LCD_WR_DATA(0x0020);
	LCD_WR_DATA(0x0004);
	LCD_WR_DATA(0x0010);
	LCD_WR_DATA(0x0008);
	LCD_WR_DATA(0x0034);
	LCD_WR_DATA(0x0047);
	LCD_WR_DATA(0x0044);
	LCD_WR_DATA(0x0005);
	LCD_WR_DATA(0x000b);
	LCD_WR_DATA(0x0009);
	LCD_WR_DATA(0x002f);
	LCD_WR_DATA(0x0036);
	LCD_WR_DATA(0x000f);

	// Change adresses

	// Column
	LCD_WR_REG(0x002A);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0001);
	LCD_WR_DATA(0x003F);

	// Row
	LCD_WR_REG(0x002B);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x0000);
	LCD_WR_DATA(0x00EF);

	LCD_WR_REG(0x003A);
	LCD_WR_DATA(0x0055);

	LCD_WR_REG(0x00f6);
	LCD_WR_DATA(0x0001);
	LCD_WR_DATA(0x0030);
	LCD_WR_DATA(0x0000);

	// Change MADCTL
	// Set LCD in reverse mode (row/column) exchange:  MADCTL B5='1'
	// BGR: MADCTL B3='1'
	LCD_WR_REG(0x0036);

	int32_t MH = 1 << 2;
	int32_t BGR = 1 << 3;
	int32_t ML = 1 << 4;
	int32_t MV = 1 << 5;
	int32_t MX = 1 << 6;
	int32_t MY = 1 << 7;
	LCD_WR_DATA(MH + BGR + ML + MV + MY);

	LCD_WR_REG(0x0029); //display on

	printf("INIT DONE \n");
}

void set_dma_transfer(uint32_t addr, uint32_t burst_length) {
	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x20, addr);
	uint32_t end_addr = addr + FRAME_LENGTH - burst_length;
	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x24, end_addr);
	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x28, burst_length);
	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x2C, FIFO_LENGTH-burst_length);
	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x30, 0x1);
	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x1C, 0x1);
}

void report_dma() {
	printf("------------------------\n");
	printf("Operation Mode = 0x%01x.\n",
			IORD_32DIRECT(LCD_CONTROLLER_0_BASE, 0x30));
	printf("LCD FSM Status = 0x%01x.\n",
			IORD_32DIRECT(LCD_CONTROLLER_0_BASE, 0x18));
	printf("DMA FSM Status = 0x%01x.\n",
			IORD_32DIRECT(LCD_CONTROLLER_0_BASE, 0x1C));
	printf("DMA Address    = 0x%08x.\n",
			IORD_32DIRECT(LCD_CONTROLLER_0_BASE, 0x20));
	printf("DMA RX CNT     = 0x%08x.\n",
			IORD_32DIRECT(LCD_CONTROLLER_0_BASE, 0x24));
	printf("DMA BURST CNT  = 0x%08x.\n",
			IORD_32DIRECT(LCD_CONTROLLER_0_BASE, 0x28));
	printf("DMA FIFO THRSD = 0x%08x.\n",
			IORD_32DIRECT(LCD_CONTROLLER_0_BASE, 0x2C));
}

void Load_pics_train() {
	printf("loading pics train\n");
	RAM_Init_Pic(0 * 4 * 160 * 240, "/mnt/host/bins/frame_0_delay-0.04s.bin");
	RAM_Init_Pic(1 * 4 * 160 * 240, "/mnt/host/bins/frame_1_delay-0.04s.bin");
	RAM_Init_Pic(2 * 4 * 160 * 240, "/mnt/host/bins/frame_2_delay-0.04s.bin");
	RAM_Init_Pic(3 * 4 * 160 * 240, "/mnt/host/bins/frame_3_delay-0.04s.bin");
	RAM_Init_Pic(4 * 4 * 160 * 240, "/mnt/host/bins/frame_4_delay-0.04s.bin");
	RAM_Init_Pic(5 * 4 * 160 * 240, "/mnt/host/bins/frame_5_delay-0.04s.bin");
	RAM_Init_Pic(6 * 4 * 160 * 240, "/mnt/host/bins/frame_6_delay-0.04s.bin");
	RAM_Init_Pic(7 * 4 * 160 * 240, "/mnt/host/bins/frame_7_delay-0.04s.bin");
	RAM_Init_Pic(8 * 4 * 160 * 240, "/mnt/host/bins/frame_8_delay-0.04s.bin");
	RAM_Init_Pic(9 * 4 * 160 * 240, "/mnt/host/bins/frame_9_delay-0.04s.bin");
	RAM_Init_Pic(10 * 4 * 160 * 240, "/mnt/host/bins/frame_10_delay-0.04s.bin");
	RAM_Init_Pic(11 * 4 * 160 * 240, "/mnt/host/bins/frame_11_delay-0.04s.bin");
	RAM_Init_Pic(12 * 4 * 160 * 240, "/mnt/host/bins/frame_12_delay-0.04s.bin");
	RAM_Init_Pic(13 * 4 * 160 * 240, "/mnt/host/bins/frame_13_delay-0.04s.bin");
	RAM_Init_Pic(14 * 4 * 160 * 240, "/mnt/host/bins/frame_14_delay-0.04s.bin");
	RAM_Init_Pic(15 * 4 * 160 * 240, "/mnt/host/bins/frame_15_delay-0.04s.bin");
	RAM_Init_Pic(16 * 4 * 160 * 240, "/mnt/host/bins/frame_16_delay-0.04s.bin");
	RAM_Init_Pic(17 * 4 * 160 * 240, "/mnt/host/bins/frame_17_delay-0.04s.bin");
	RAM_Init_Pic(18 * 4 * 160 * 240, "/mnt/host/bins/frame_18_delay-0.04s.bin");
	RAM_Init_Pic(19 * 4 * 160 * 240, "/mnt/host/bins/frame_19_delay-0.04s.bin");
	RAM_Init_Pic(20 * 4 * 160 * 240, "/mnt/host/bins/frame_20_delay-0.04s.bin");
	RAM_Init_Pic(21 * 4 * 160 * 240, "/mnt/host/bins/frame_21_delay-0.04s.bin");
	RAM_Init_Pic(22 * 4 * 160 * 240, "/mnt/host/bins/frame_22_delay-0.04s.bin");
	RAM_Init_Pic(23 * 4 * 160 * 240, "/mnt/host/bins/frame_23_delay-0.04s.bin");

	printf("all pics train loaded\n");
}

void Load_pics_homealone() {
	printf("loading pics home alone\n");
	RAM_Init_Pic(0 * 4 * 160 * 240, "/mnt/host/bins/frame_0_delay-0.1s.bin");
	RAM_Init_Pic(1 * 4 * 160 * 240, "/mnt/host/bins/frame_1_delay-0.1s.bin");
	RAM_Init_Pic(2 * 4 * 160 * 240, "/mnt/host/bins/frame_2_delay-0.1s.bin");
	RAM_Init_Pic(3 * 4 * 160 * 240, "/mnt/host/bins/frame_3_delay-0.1s.bin");
	RAM_Init_Pic(4 * 4 * 160 * 240, "/mnt/host/bins/frame_4_delay-0.1s.bin");
	RAM_Init_Pic(5 * 4 * 160 * 240, "/mnt/host/bins/frame_5_delay-0.1s.bin");
	RAM_Init_Pic(6 * 4 * 160 * 240, "/mnt/host/bins/frame_6_delay-0.1s.bin");
	RAM_Init_Pic(7 * 4 * 160 * 240, "/mnt/host/bins/frame_7_delay-0.1s.bin");
	RAM_Init_Pic(8 * 4 * 160 * 240, "/mnt/host/bins/frame_8_delay-0.1s.bin");
	RAM_Init_Pic(9 * 4 * 160 * 240, "/mnt/host/bins/frame_9_delay-0.1s.bin");
	RAM_Init_Pic(10 * 4 * 160 * 240, "/mnt/host/bins/frame_10_delay-0.1s.bin");
	RAM_Init_Pic(11 * 4 * 160 * 240, "/mnt/host/bins/frame_11_delay-0.1s.bin");
	RAM_Init_Pic(12 * 4 * 160 * 240, "/mnt/host/bins/frame_12_delay-0.1s.bin");
	RAM_Init_Pic(13 * 4 * 160 * 240, "/mnt/host/bins/frame_13_delay-0.1s.bin");
	RAM_Init_Pic(14 * 4 * 160 * 240, "/mnt/host/bins/frame_14_delay-0.1s.bin");
	RAM_Init_Pic(15 * 4 * 160 * 240, "/mnt/host/bins/frame_15_delay-0.1s.bin");
	RAM_Init_Pic(16 * 4 * 160 * 240, "/mnt/host/bins/frame_16_delay-0.1s.bin");

	printf("all pics home alone loaded\n");
}

void Load_pics_spyderman() {
	printf("loading pics spyderman\n");
	RAM_Init_Pic(0 * 4 * 160 * 240, "/mnt/host/bins/frame_0_delay-0.03s.bin");
	RAM_Init_Pic(1 * 4 * 160 * 240, "/mnt/host/bins/frame_1_delay-0.03s.bin");
	RAM_Init_Pic(2 * 4 * 160 * 240, "/mnt/host/bins/frame_2_delay-0.03s.bin");
	RAM_Init_Pic(3 * 4 * 160 * 240, "/mnt/host/bins/frame_3_delay-0.03s.bin");
	RAM_Init_Pic(4 * 4 * 160 * 240, "/mnt/host/bins/frame_4_delay-0.03s.bin");
	RAM_Init_Pic(5 * 4 * 160 * 240, "/mnt/host/bins/frame_5_delay-0.03s.bin");
	RAM_Init_Pic(6 * 4 * 160 * 240, "/mnt/host/bins/frame_6_delay-0.03s.bin");
	RAM_Init_Pic(7 * 4 * 160 * 240, "/mnt/host/bins/frame_7_delay-0.03s.bin");
	RAM_Init_Pic(8 * 4 * 160 * 240, "/mnt/host/bins/frame_8_delay-0.03s.bin");
	RAM_Init_Pic(9 * 4 * 160 * 240, "/mnt/host/bins/frame_9_delay-0.03s.bin");
	RAM_Init_Pic(10 * 4 * 160 * 240, "/mnt/host/bins/frame_10_delay-0.03s.bin");
	RAM_Init_Pic(11 * 4 * 160 * 240, "/mnt/host/bins/frame_11_delay-0.03s.bin");
	RAM_Init_Pic(12 * 4 * 160 * 240, "/mnt/host/bins/frame_12_delay-0.03s.bin");
	RAM_Init_Pic(13 * 4 * 160 * 240, "/mnt/host/bins/frame_13_delay-0.03s.bin");
	RAM_Init_Pic(14 * 4 * 160 * 240, "/mnt/host/bins/frame_14_delay-0.03s.bin");

	printf("all pics spyderman loaded\n");
}

void init_pointers() {
	for (uint32_t i = 0; i < N_BUFFERS; i += 1) {
		pointers[i] = HPS_0_BRIDGES_BASE + i * FRAME_LENGTH;
	}
}

uint32_t get_new_addr() {
	if (p == N_BUFFERS - 1)
		p = 0;
	else
		p++;
	return pointers[p];
}

void prog_delay() {
	uint32_t keyn = IORD_32DIRECT(LCD_CONTROLLER_0_BASE, 0x34);
	int32_t step = frame_int >> 5;
	if (step < 1) {step = 1;}
	if (keyn == 0x2) {
		frame_int -= step;
	}
	else if (keyn == 0x1) {
		frame_int+= step;
	}
	if (frame_int < 0) {frame_int = 0;}
	Delay_Us(frame_int);
}

static void irqhandler(void * context) {

	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x18, 0x1); // ack irq
	prog_delay();
	uint32_t addr = get_new_addr();
	set_dma_transfer(addr, BURST_LENGTH);

}

int main(void) {
	init_pointers();
	// registering IRQ
	alt_irq_register( LCD_CONTROLLER_0_IRQ_INTERRUPT_CONTROLLER_ID, NULL,
			(void*) irqhandler);
	// testing memory
	//memtst(4*ONE_MB);
	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x30, 0x0);
	set_lcd_on(0x0);
	set_lcd_on(0x1);
	//lcd_init();
	LCD_Init();
	IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x30, 0x0);
	LCD_WR_REG(0x002c); // Start Frame Memory write
	set_dma_transfer(get_new_addr(), BURST_LENGTH);
	Load_pics_train();
	for (;;) {
	} // Do nothing forever
	//set_dma_transfer(0x0, 0x4);
	//IOWR_32DIRECT(LCD_CONTROLLER_0_BASE, 0x30, 0x0);
	//report_lcd_info();
//	fill_frame(0x001F);
//	read_frame_mem();
//	fill_frame(0x07E0);
//	read_frame_mem();
//	fill_frame(0xF800);

	return EXIT_SUCCESS;
}

