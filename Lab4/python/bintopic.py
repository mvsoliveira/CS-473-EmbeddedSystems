from helpers import *
import os

for i in os.listdir(os.getcwd()+'\\bins'):
    print i
    path_bin = './bins/'+i
    path_pic = './pics_check/'+'.'.join(i.split('.')[:-1])+'.png'
    lt24_rec = from_file(path_bin)
    arr_rec = from_lt24 (lt24_rec)
    img_rec = img_from_array(arr_rec)
    save_image(img_rec, path_pic)
    img_rec.show() #not needed just quicker
