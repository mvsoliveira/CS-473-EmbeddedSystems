from helpers import *
import os

for i in os.listdir(os.getcwd()+'\\pics'):
	print i
	path_pic = './pics/'+i
	path_bin = './bins/'+'.'.join(i.split('.')[:-1])+'.bin'
	img = load_image(path_pic)
	arr = array_from_img(img)
	lt24 = to_lt24(arr)
	to_file(lt24, path_bin)
