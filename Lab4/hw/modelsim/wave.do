onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /lcd_top_tb/nReset
add wave -noupdate -radix unsigned /lcd_top_tb/AS_Address
add wave -noupdate /lcd_top_tb/AS_ChipSelect
add wave -noupdate /lcd_top_tb/AS_WaitRequest
add wave -noupdate /lcd_top_tb/AS_IRQ
add wave -noupdate /lcd_top_tb/AS_Read
add wave -noupdate /lcd_top_tb/AS_Write
add wave -noupdate -radix hexadecimal /lcd_top_tb/AS_ReadData
add wave -noupdate -radix hexadecimal /lcd_top_tb/AS_WriteData
add wave -noupdate /lcd_top_tb/Clk
add wave -noupdate /lcd_top_tb/CSx
add wave -noupdate /lcd_top_tb/DCx
add wave -noupdate -color {Violet Red} /lcd_top_tb/WRx
add wave -noupdate -color Orange /lcd_top_tb/RDx
add wave -noupdate /lcd_top_tb/DUT/lcd_control_1/D_tri
add wave -noupdate -radix hexadecimal /lcd_top_tb/D
add wave -noupdate /lcd_top_tb/DUT/lcd_control_1/ReadFSM
add wave -noupdate /lcd_top_tb/DUT/lcd_control_1/WriteFSM
add wave -noupdate /lcd_top_tb/DUT/lcd_control_1/state
add wave -noupdate -radix unsigned /lcd_top_tb/DUT/lcd_control_1/cnt
add wave -noupdate /lcd_top_tb/DUT/lcd_control_1/ReadDataFSM
add wave -noupdate /lcd_top_tb/DUT/lcd_control_1/D_in
add wave -noupdate /lcd_top_tb/DUT/LCD_ON
add wave -noupdate /lcd_top_tb/DUT/RSx
add wave -noupdate /lcd_top_tb/Clk
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/AM_Address
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/AM_ByteEnable
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/AM_BurstCount
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/AM_Read
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/AM_WaitRequest
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/AM_ReadValid
add wave -noupdate /lcd_top_tb/DUT/dma_rx_d
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/AM_ReadData
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/regDmaFire
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/regDmaStartAddr
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/regDmaEndAddr
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/regDmaBurstCount
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/regDmaFifoThreshold
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/regDmaIrqAck
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/regOpMode
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/dma_addr
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/dma_irq
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/dma_s
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/dma_rx_cnt
add wave -noupdate /lcd_top_tb/DUT/fifo_wrreq
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/fifo_data
add wave -noupdate /lcd_top_tb/DUT/fifo_rdclk
add wave -noupdate /lcd_top_tb/DUT/fifo_rdreq
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/fifo_q
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/fifo_rdusedw
add wave -noupdate -radix hexadecimal /lcd_top_tb/DUT/fifo_wrusedw
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {4320811 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 305
configure wave -valuecolwidth 160
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {2651869 ps} {3449488 ps}
