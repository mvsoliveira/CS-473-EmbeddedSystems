transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {C:/cs473/Lab4/hw/quartus/lcd_top/fifo.vhd}
vcom -93 -work work {C:/cs473/Lab4/hw/hdl/lcd_control.vhd}
vcom -93 -work work {C:/cs473/Lab4/hw/hdl/lcd_top.vhd}
vcom -93 -work work {C:/cs473/Lab4/hw/quartus/lcd_top/../../modelsim/lcd_top_tb.vhd}


