-------------------------------------------------------------------------------
-- Title      : Testbench for design "lcd_top"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : lcd_top_tb.vhd
-- Author     :   <msilvaol@PCPHL1CT05>
-- Company    : 
-- Created    : 2016-12-22
-- Last update: 2017-01-07
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2016 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-12-22  1.0      msilvaol        Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity lcd_top_tb is

end entity lcd_top_tb;

-------------------------------------------------------------------------------

architecture tb of lcd_top_tb is

  -- component ports
  signal Clk            : std_logic;
  signal nReset         : std_logic;
  signal AS_Address     : std_logic_vector (3 downto 0);
  signal AS_ChipSelect  : std_logic;
  signal AS_WaitRequest : std_logic;
  signal AS_IRQ         : std_logic;
  signal AS_Read        : std_logic;
  signal AS_Write       : std_logic;
  signal AS_ReadData    : std_logic_vector (31 downto 0);
  signal AS_WriteData   : std_logic_vector (31 downto 0);
  signal LCD_ON         : std_logic;
  signal KEY_N          : std_logic_vector(1 downto 0);
  signal IRQAck         : std_logic;
  signal RSx            : std_logic;
  signal CSx            : std_logic;
  signal DCx            : std_logic;
  signal WRx            : std_logic;
  signal RDx            : std_logic;
  signal D              : std_logic_vector(15 downto 0);


  signal AM_Address     : std_logic_vector (31 downto 0);
  signal AM_ByteEnable  : std_logic_vector(3 downto 0);
  signal AM_BurstCount  : std_logic_vector(10 downto 0);
  signal AM_Read        : std_logic;
  signal AM_WaitRequest : std_logic;
  signal AM_ReadValid   : std_logic;
  signal AM_ReadData    : std_logic_vector (31 downto 0);


  -- clock
  constant gbl_clk_period : time := 20 ns;

begin  -- architecture tb

  -- component instantiation
  DUT : entity work.lcd_top
    port map (
      Clk            => Clk,
      nReset         => nReset,
      AS_Address     => AS_Address,
      AS_ChipSelect  => AS_ChipSelect,
      AS_WaitRequest => AS_WaitRequest,
      AS_IRQ         => AS_IRQ,
      AS_Read        => AS_Read,
      AS_Write       => AS_Write,
      AS_ReadData    => AS_ReadData,
      AS_WriteData   => AS_WriteData,
      AM_Address     => AM_Address,
      AM_ByteEnable  => AM_ByteEnable,
      AM_BurstCount  => AM_BurstCount,
      AM_Read        => AM_Read,
      AM_WaitRequest => AM_WaitRequest,
      AM_ReadValid   => AM_ReadValid,
      AM_ReadData    => AM_ReadData,
      KEY_N          => KEY_N,
      IRQAck         => IRQAck,
      LCD_ON         => LCD_ON,
      RSx            => RSx,
      CSx            => CSx,
      DCx            => DCx,
      WRx            => WRx,
      RDx            => RDx,
      D              => D);




  -- clock generation
  gbl_clk : process
  begin
    Clk <= '0';
    wait for gbl_clk_period/2;
    Clk <= '1';
    wait for gbl_clk_period/2;
  end process;

  --D <= (others => '1');
  -- waveform generation
  WaveGen_Proc : process
  begin
    -- insert signal assignments here
    nReset        <= '1';
    wait for 100 ns;
    nReset        <= '0';
    AS_Address    <= (others => '0');
    AS_ChipSelect <= '0';
    AS_Write      <= '0';
    AS_Read       <= '0';
    AS_WriteData  <= (others => '0');

    AM_WaitRequest <= '1';
    AM_ReadValid   <= '0';
    AM_ReadData    <= (others => '0');

    wait for 20 ns;
    nReset         <= '1';
    wait for 211 ns;
    -- turning on LCD
    AS_Address     <= x"2";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_WriteData   <= x"00000001";
    wait for 20 ns;
    AS_ChipSelect  <= '0';
    AS_Address     <= (others => '0');
    AS_Write       <= '0';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000000";
    wait for 40 ns;
    -- asserting reset
    AS_Address     <= x"3";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_WriteData   <= x"00000000";
    wait for 20 ns;
    AS_ChipSelect  <= '0';
    AS_Address     <= (others => '0');
    AS_Write       <= '0';
    AS_Read        <= '0';
    wait for 40 ns;
    wait for 200 ns;
    -- reading reset
    AS_Address     <= x"3";
    AS_ChipSelect  <= '1';
    AS_Write       <= '0';
    AS_Read        <= '1';
    wait for 20 ns;
    AS_ChipSelect  <= '0';
    AS_Address     <= (others => '0');
    AS_Write       <= '0';
    AS_Read        <= '0';
    wait for 40 ns;
    -- reading LCDOn
    AS_Address     <= x"2";
    AS_ChipSelect  <= '1';
    AS_Write       <= '0';
    AS_Read        <= '1';
    wait for 20 ns;
    AS_ChipSelect  <= '0';
    AS_Address     <= (others => '0');
    AS_Write       <= '0';
    AS_Read        <= '0';
    wait for 40 ns;
    -- de-asserting reset
    AS_Address     <= x"3";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000001";
    wait for 20 ns;
    AS_ChipSelect  <= '0';
    AS_Address     <= (others => '0');
    AS_Write       <= '0';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000000";
    wait for 40 ns;
    -- writing data
    AS_Address     <= x"1";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_WriteData   <= x"00000088";
    wait for 20 ns;
    AS_ChipSelect  <= '0';
    AS_Address     <= (others => '0');
    AS_Write       <= '0';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000000";
    wait for 100 ns;
    -- reading data    
    AS_Address     <= x"1";
    AS_ChipSelect  <= '1';
    AS_Write       <= '0';
    AS_Read        <= '1';
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    wait until AS_WaitRequest = '0';
    -- writing a command
    AS_Address     <= x"0";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_WriteData   <= x"0000000F";
    wait for 100 ns;
    wait for 20 ns;
    AS_ChipSelect  <= '0';
    AS_Address     <= (others => '0');
    AS_Write       <= '0';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000000";
    wait for 25 ns;
    -- Configuring DMA start address
    AS_Address     <= x"8";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000100";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    -- Configuring DMA end address
    AS_Address     <= x"9";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000108";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    -- Configuring DMA burst count
    AS_Address     <= x"A";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000004";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    -- Configuring DMA FIFO threshold
    AS_Address     <= x"B";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000010";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    -- Firing DMA transfer 
    AS_Address     <= x"7";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000001";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    wait for 80 ns;
    AM_WaitRequest <= '0';
    wait for 200 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"00000200";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"00000201";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"00000202";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"00000203";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 100 ns;
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"00000204";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"00000205";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"00000206";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"00000207";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 100 ns;
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"00000208";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"00000209";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"0000020a";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"0000020b";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    wait for 200 ns;
    -- acknowledge IRQ
    AS_Address     <= x"6";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000001";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    wait for 200 ns;
    -- Configuring DMA start address
    AS_Address     <= x"8";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000200";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    -- Configuring DMA end address
    AS_Address     <= x"9";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000208";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    -- Configuring DMA burst count
    AS_Address     <= x"A";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000004";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    -- Configuring DMA FIFO threshold
    AS_Address     <= x"B";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000010";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    -- Masking IRQ
    AS_Address     <= x"5";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000000";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    -- Firing DMA transfer 
    AS_Address     <= x"7";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000001";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    wait for 200 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"10000200";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"10000201";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"10000202";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"10000203";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 100 ns;
    -- Switching Operation mode to DMA 
    AS_Address     <= x"C";
    AS_ChipSelect  <= '1';
    AS_Write       <= '1';
    AS_Read        <= '0';
    AS_WriteData   <= x"00000001";
    wait for 20 ns;
    AS_Address     <= x"0";
    AS_ChipSelect  <= '0';
    AS_Write       <= '0';
    AS_Read        <= '0';
    wait for 500 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"10000204";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"10000205";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"10000206";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"10000207";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 100 ns;
    wait for 500 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"10000208";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"10000209";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"1000020A";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";
    wait for 20 ns;
    wait for 3 us;
    AM_ReadValid   <= '1';
    AM_ReadData    <= x"1000020B";
    wait for 20 ns;
    AM_ReadValid   <= '0';
    AM_ReadData    <= x"00000000";

    wait for 100 ns;




    wait;
  end process WaveGen_Proc;






end architecture tb;

-------------------------------------------------------------------------------

configuration lcd_top_tb_tb_cfg of lcd_top_tb is
  for tb
  end for;
end lcd_top_tb_tb_cfg;

-------------------------------------------------------------------------------
