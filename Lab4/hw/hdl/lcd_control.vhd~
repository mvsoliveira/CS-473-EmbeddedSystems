----------------------------------------------------------------------------------------------------------------------
-- Title      : LCD control unit
-- Project    : MUCTPI

----------------------------------------------------------------------------------------------------------------------
-- File       : lcd_control.vhd
-- Author     : Marcos Oliveira
-- Company    : CERN
-- Created    : 2016-12-21
-- Last update: 2017-01-06
-- Platform   : Symplify Premier G-2012.09 and Mentor Modelsim SE-64 10.1c
-- Standard   : VHDL'93/02
----------------------------------------------------------------------------------------------------------------------
-- Description: Controls the LCD driver from an Avalon Slave interface
----------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2016 CERN
----------------------------------------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-12-21  1.0      msilvaol        Created
----------------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

entity lcd_control is

  port (
    clk          : in    std_logic;
    nReset       : in    std_logic;
    ReadFSM      : in    std_logic;
    WriteFSM     : in    std_logic;
    ReadDataFSM  : out   std_logic_vector (15 downto 0);
    WriteDataFSM : in    std_logic_vector (15 downto 0);
    waitrequest  : out   std_logic;
    -- debug
    state_debug  : out std_logic_vector(3 downto 0); 
    -- DMA operation mode signals
    OpMode       : in    std_logic;
    fifo_q       : in    std_logic_vector(15 downto 0);
    fifo_rdusedw : in    std_logic_vector (8 downto 0);
    fifo_rdreq   : out   std_logic;
    --lcd
    CSx          : out   std_logic;
    WRx          : out   std_logic;
    RDx          : out   std_logic;
    D            : inout std_logic_vector(15 downto 0)
    );

end entity lcd_control;

architecture rtl of lcd_control is

  -- Build an enumerated type for the state machine
  type state_type is (IDDLE, WRITE, WRITE_WAIT, READ, READ_L_WAIT, EDGE_W, EDGE_R, FINISH_W, FINISH_R);

  -- Register to hold the current state
  signal state : state_type;

  -- tristate buffer signals
  signal D_tri          : std_logic;
  signal D_in           : std_logic_vector(15 downto 0);
  signal D_out          : std_logic_vector(15 downto 0);
  -- lcd control
  signal cnt            : unsigned(3 downto 0);
  signal EdgeLast       : std_logic;
  signal Edge1          : std_logic;
  signal waitrequestFSM : std_logic;


begin  -- architecture rtl

  state_debug <= std_logic_vector(to_unsigned(state_type'pos(state),4));

  ----------------------------------------------------------------------------------------------------------------------
  -- Tri state buffer
  ----------------------------------------------------------------------------------------------------------------------
  D_out <= WriteDataFSM when OpMode = '0' else fifo_q;
  D     <= D_out        when D_tri = '1'  else (others => 'Z');
  D_in  <= D;

------------------------------------------------------------------------------------------------------------------------
-- State machine
------------------------------------------------------------------------------------------------------------------------

  -- Logic to advance to the next state
  process (clk, nReset)
  begin
    if nReset = '0' then
      state       <= IDDLE;
      ReadDataFSM <= (others => '0');
      fifo_rdreq  <= '0';
    elsif (rising_edge(clk)) then
      -- default value
      fifo_rdreq <= '0';
      case state is
        when IDDLE =>
          if OpMode = '0' then          -- if OpMode = 0
            if WriteFSM = '1' then
              state <= WRITE;
            elsif ReadFSM = '1' then
              state <= READ;
            end if;
          else                          -- if OpMode = 1
            if unsigned(fifo_rdusedw) > 1 then
              --fifo_rdreq <= '1';
              state <= WRITE;
            end if;
          end if;
          cnt      <= (others => '0');
          Edge1    <= '0';
          EdgeLast <= '0';
        when WRITE =>
          state    <= WRITE_WAIT;
          EdgeLast <= '1';
        when WRITE_WAIT =>
          state <= EDGE_W;
        when READ =>
          state    <= READ_L_WAIT;
          cnt      <= (others => '0');
          --EdgeLast <= Edge1;
          EdgeLast <= '1';              -- pushing dummy read to SW
        when READ_L_WAIT =>
          if cnt = x"F" then
            state                    <= EDGE_R;
            ReadDataFSM(15 downto 0) <= D_in;
          else
            cnt <= cnt + 1;
          end if;
        -- when READ_H_WAIT =>
        --   if cnt = x"F" then
        --     state <= READ;
        --   else
        --     cnt <= cnt + 1;
        --   end if;

        when EDGE_W =>
          if OpMode = '0' then
            state <= FINISH_W;
          else
            if unsigned(fifo_rdusedw) > 1 then
              fifo_rdreq <= '1';
              state      <= FINISH_W;
            end if;
          end if;

        when EDGE_R =>
          --if EdgeLast = '1' then
            state <= FINISH_R;
          --else
          --  state <= READ_H_WAIT;
          --end if;
          cnt   <= (others => '0');
          --Edge1 <= '1';

        when FINISH_W =>
          if OpMode = '0' then
            state <= IDDLE;
          else
            state <= WRITE;
          end if;
        when FINISH_R =>
          state <= IDDLE;
      end case;
    end if;
  end process;


  -- combinatorial output
  process (state)
  begin

    case state is
      when IDDLE =>
        waitrequestFSM <= '0';
        --CSx            <= '1';
        CSx            <= '0';          -- trying to keep CSx low all the time
        WRx            <= '1';
        RDx            <= '1';
        D_tri          <= '0';

      when WRITE =>
        waitrequestFSM <= '1';
        CSx            <= '0';
        WRx            <= '0';
        RDx            <= '1';
        D_tri          <= '1';

      when WRITE_WAIT =>
        waitrequestFSM <= '1';
        CSx            <= '0';
        WRx            <= '0';
        RDx            <= '1';
        D_tri          <= '1';

      when READ =>
        waitrequestFSM <= '1';
        CSx            <= '0';
        WRx            <= '1';
        RDx            <= '0';
        D_tri          <= '0';

      when READ_L_WAIT =>
        waitrequestFSM <= '1';
        CSx            <= '0';
        WRx            <= '1';
        RDx            <= '0';
        D_tri          <= '0';

      -- when READ_H_WAIT =>
      --   waitrequestFSM <= '1';
      --   CSx            <= '0';
      --   WRx            <= '1';
      --   RDx            <= '1';
      --   D_tri          <= '0';

      when EDGE_W =>
        waitrequestFSM <= '1';
        CSx            <= '0';
        WRx            <= '1';
        RDx            <= '1';
        D_tri          <= '1';

      when EDGE_R =>
        waitrequestFSM <= '1';
        CSx            <= '0';
        WRx            <= '1';
        RDx            <= '1';
        D_tri          <= '0';

      when FINISH_W =>
        waitrequestFSM <= '0';
        CSx            <= '0';
        WRx            <= '1';
        RDx            <= '1';
        D_tri          <= '1';

      when FINISH_R =>
        waitrequestFSM <= '0';
        CSx            <= '0';
        WRx            <= '1';
        RDx            <= '1';
        D_tri          <= '0';

      when others =>
        waitrequestFSM <= '0';
        CSx            <= '0';
        WRx            <= '1';
        RDx            <= '1';
        D_tri          <= '0';


    end case;
  end process;


------------------------------------------------------------------------------------------------------------------------
-- Combinatorial outputs
------------------------------------------------------------------------------------------------------------------------

  waitrequest <= waitrequestFSM when OpMode = '0' else '0';


end architecture rtl;


