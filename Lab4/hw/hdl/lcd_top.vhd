----------------------------------------------------------------------------------------------------------------------
-- Title      : LCD top level
-- Project    : CS-473 Embedded Systems
----------------------------------------------------------------------------------------------------------------------
-- File       : lcd_top.vhd
-- Author     : Marcos Oliveira
-- Company    : CERN
-- Created    : 2016-12-21
-- Last update: 2017-01-07
-- Platform   : Symplify Premier G-2012.09 and Mentor Modelsim SE-64 10.1c
-- Standard   : VHDL'93/02
----------------------------------------------------------------------------------------------------------------------
-- Description: LCD tope level
-------------------------------------------------------------------------------
-- Copyright (c) 2016 CERN
----------------------------------------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-12-21  1.0      msilvaol        Created
----------------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lcd_top is

  port (
    -- Clock and reset
    Clk            : in    std_logic;
    nReset         : in    std_logic;
    -- Avalon slave interfaces signals
    AS_Address     : in    std_logic_vector (3 downto 0);
    AS_ChipSelect  : in    std_logic;
    AS_WaitRequest : out   std_logic;
    AS_IRQ         : out   std_logic;
    AS_Read        : in    std_logic;
    AS_Write       : in    std_logic;
    AS_ReadData    : out   std_logic_vector (31 downto 0);
    AS_WriteData   : in    std_logic_vector (31 downto 0);
    -- Avalon Master Signals
    AM_Address     : out   std_logic_vector (31 downto 0);
    AM_ByteEnable  : out   std_logic_vector(3 downto 0);
    AM_BurstCount  : out   std_logic_vector(10 downto 0);
    AM_Read        : out   std_logic;
    AM_WaitRequest : in    std_logic;
    AM_ReadValid   : in    std_logic;
    AM_ReadData    : in    std_logic_vector (31 downto 0);
    -- Key
    KEY_N          : in    std_logic_vector(1 downto 0);
    -- IRQAck
    IRQAck         : out   std_logic;
    --  LCD driver
    LCD_ON         : out   std_logic;
    RSx            : out   std_logic;
    CSx            : out   std_logic;
    DCx            : out   std_logic;
    WRx            : out   std_logic;
    RDx            : out   std_logic;
    D              : inout std_logic_vector(15 downto 0)
    );
end lcd_top;

architecture rtl of lcd_top is


  -- lcd control
  signal ReadFSM      : std_logic;
  signal WriteFSM     : std_logic;
  signal ReadDataFSM  : std_logic_vector (15 downto 0);
  signal WriteDataFSM : std_logic_vector (15 downto 0);
  signal regLCDData   : std_logic_vector (15 downto 0);

  signal waitrequest         : std_logic;
  -- avalon registers
  signal regLCDOn            : std_logic;
  signal regLCDRSx           : std_logic;
  signal regDmaFire          : std_logic;
  signal regDmaStartAddr     : unsigned(31 downto 0);
  signal regDmaEndAddr       : unsigned(31 downto 0);
  signal regDmaBurstCount    : unsigned(10 downto 0);
  signal regDmaFifoThreshold : unsigned(7 downto 0);
  signal regDmaIrqAck        : std_logic;
  signal regMaskIRQ          : std_logic;
  signal regOpMode           : std_logic;
  signal regDmaSyncRst       : std_logic;
  -- interruption
  signal irq                 : std_logic;
  -- dma
  signal dma_addr            : unsigned(31 downto 0);
  signal dma_irq             : std_logic;
  signal dma_rx_cnt          : unsigned(10 downto 0);
  signal dma_rx_d            : std_logic;
  type dma_s_t is (waiting, avalon, transfering, irqreq);
  signal dma_s               : dma_s_t;
  --fifo
  signal fifo_data           : std_logic_vector (31 downto 0);
  signal fifo_rdclk          : std_logic;
  signal fifo_rdreq          : std_logic;
  signal fifo_wrclk          : std_logic;
  signal fifo_wrreq          : std_logic;
  signal fifo_q              : std_logic_vector(15 downto 0);

  signal fifo_rdusedw : std_logic_vector(8 downto 0);
  signal fifo_wrusedw : std_logic_vector(7 downto 0);

  signal lcd_state_debug : std_logic_vector(3 downto 0);

begin  -- architecture rtl

  ----------------------------------------------------------------------------------------------------------------------
  -- Main Logic
  ----------------------------------------------------------------------------------------------------------------------

  main : process (Clk, nReset)
  begin
    if nReset = '0' then
      regLCDOn            <= '0';
      regLCDRSx           <= '0';
      regLCDData          <= (others => '0');
      regDmaFire          <= '0';
      regDmaStartAddr     <= (others => '0');
      regDmaEndAddr       <= (others => '0');
      regDmaBurstCount    <= (others => '0');
      regDmaFifoThreshold <= (others => '0');
      regDmaIrqAck        <= '0';
      regMaskIRQ          <= '0';
      regOpMode           <= '0';
      AS_ReadData         <= (others => '0');
      DCx                 <= '0';
      irq                 <= '0';
      dma_addr            <= (others => '0');
      dma_rx_cnt          <= (others => '0');
      dma_rx_d            <= '0';
      dma_irq             <= '0';
      AM_Address          <= (others => '0');
      AM_ByteEnable       <= (others => '0');
      AM_BurstCount       <= (others => '0');
      AM_Read             <= '0';
      dma_s               <= waiting;
      regDmaSyncRst       <= '0';

    elsif rising_edge(Clk) then
      -------------------------------------------------------------------------
      -- Write logic
      -------------------------------------------------------------------------
      -- self clear values
      regDmaIrqAck  <= '0';
      regDmaFire    <= '0';
      regDmaSyncRst <= '0';
      if AS_ChipSelect = '1' and AS_Write = '1' and waitrequest = '0' then  -- Write cycle

        case AS_Address(3 downto 0) is
          when x"0" | x"1" =>                          -- lcd cmd
            regLCDData <= AS_WriteData(15 downto 0);
            DCx        <= AS_Address(0);
          when x"2" =>                                 -- lcdon
            regLCDOn <= AS_WriteData(0);
          when x"3" =>                                 -- lcd reset
            regLCDRSx <= AS_WriteData(0);
          when x"4" =>
            null;
          when x"5" =>                                 -- force irq register
            regMaskIRQ <= AS_WriteData(1);
            irq        <= AS_WriteData(0);
          when x"6" =>
            regDmaIrqAck <= '1';
          when x"7" =>
            regDmaSyncRst <= AS_WriteData(1);
            regDmaFire    <= AS_WriteData(0);
          when x"8" =>
            regDmaStartAddr <= unsigned(AS_WriteData);
          when x"9" =>
            regDmaEndAddr <= unsigned(AS_WriteData);
          when x"A" =>
            regDmaBurstCount <= unsigned(AS_WriteData(10 downto 0));
          when x"B" =>
            regDmaFifoThreshold <= unsigned(AS_WriteData(7 downto 0));
          when x"C" =>
            regOpMode <= AS_WriteData(0);
          when others => null;
        end case;
      end if;
      if regOpMode = '1' then
        DCx <= '1';
      end if;
      -------------------------------------------------------------------------
      -- Read logic
      -------------------------------------------------------------------------  
      AS_ReadData <= (others => '0');
      if (AS_ChipSelect = '1' and AS_Read = '1') then  -- Read cycle
        -- default values
        case AS_Address(3 downto 0) is
          when x"0" =>                                 -- lcd cmd
            null;
          -- command is only writable 
          when x"1" =>                                 -- lcd data
            DCx <= '1';
          when x"2" =>
            AS_ReadData(0) <= regLCDOn;
          when x"3" =>
            AS_ReadData(0) <= regLCDRSx;
          when x"4" =>                                 -- lcd read data
            AS_ReadData(15 downto 0) <= ReadDataFSM;
          when x"5" =>
            AS_ReadData(1) <= regMaskIRQ;
            AS_ReadData(0) <= irq;
          when x"6" =>
            AS_ReadData(3 downto 0) <= lcd_state_debug;
          when x"7" =>
            AS_ReadData(1 downto 0) <= std_logic_vector(to_unsigned(dma_s_t'pos(dma_s), 2));
          when x"8" =>
            AS_ReadData <= std_logic_vector(dma_addr);
          when x"9" =>
            AS_ReadData(10 downto 0) <= std_logic_vector(dma_rx_cnt);
          when x"A" =>
            AS_ReadData(10 downto 0) <= std_logic_vector(regDmaBurstCount);
          when x"B" =>
            AS_ReadData(7 downto 0) <= std_logic_vector(regDmaFifoThreshold);
          when x"C" =>
            AS_ReadData(0) <= regOpMode;
          when x"D" =>
            AS_ReadData(1 downto 0) <= KEY_N;
          when others => null;
        end case;
      end if;

      -------------------------------------------------------------------------
      -- DMA
      -------------------------------------------------------------------------
      -- default values
      AM_Address    <= (others => '0');
      AM_ByteEnable <= (others => '0');
      AM_BurstCount <= (others => '0');
      AM_Read       <= '0';
      dma_irq       <= '0';
      case dma_s is
        when waiting =>  -- waiting for firing the whole DMA frame transfer
          if regDmaFire = '1' then
            dma_s    <= avalon;
            dma_addr <= regDmaStartAddr;
          end if;
        when avalon =>                  -- issuing DMA burst transfer
          AM_Address    <= std_logic_vector(dma_addr);
          AM_ByteEnable <= (others => '1');
          AM_BurstCount <= std_logic_vector(regDmaBurstCount);
          AM_Read       <= '1';
          if AM_WaitRequest = '0' then  -- the command was successfully sent?
            dma_s      <= transfering;
            dma_rx_cnt <= (others => '0');
          end if;
        when transfering =>
          if dma_rx_cnt >= regDmaBurstCount then  -- transfer received
            if unsigned(fifo_wrusedw) < regDmaFifoThreshold then  -- there is space?
              if dma_addr >= regDmaEndAddr then  -- is the whole frame finished?
                dma_s <= irqreq;
              else       -- frame not finished, new transfer then
                dma_s    <= avalon;
                dma_addr <= dma_addr + regDmaBurstCount;
              end if;
            end if;
          elsif dma_rx_d = '1' then  -- if transfer not complete and data comes
            dma_rx_cnt <= dma_rx_cnt + 1;
          end if;
        when irqreq =>                  -- issuing interruption request
          dma_irq <= '1';
          if regDmaIrqAck = '1' then    -- is the interruption acknowleged?
            dma_s <= waiting;
          end if;
        when others => null;
      end case;
      -- delaying cnt value for aligning with fifousedw
      dma_rx_d <= AM_ReadValid;
      -- sync reset DMA
      if regDmaSyncRst = '1' then
        dma_s <= waiting;
      end if;
    end if;
  end process main;



  WriteFSM       <= '1' when (AS_ChipSelect = '1' and AS_Write = '1' and AS_Address(3 downto 1) = "000") else '0';
  ReadFSM        <= '1' when (AS_ChipSelect = '1' and AS_Read = '1' and AS_Address(3 downto 1) = "000")  else '0';
  AS_IRQ         <= (irq or dma_irq) and not regMaskIRQ;
  IRQAck         <= regDmaIrqAck;
  AS_WaitRequest <= waitrequest;
  LCD_ON         <= regLCDOn;
  RSx            <= regLCDRSx;


  ----------------------------------------------------------------------------------------------------------------------
  -- LCD control
  ----------------------------------------------------------------------------------------------------------------------



  lcd_control_1 : entity work.lcd_control
    port map (
      clk          => Clk,
      nReset       => nReset,
      ReadFSM      => ReadFSM,
      WriteFSM     => WriteFSM,
      ReadDataFSM  => ReadDataFSM,
      WriteDataFSM => regLCDData,
      waitrequest  => waitrequest,
      OpMode       => regOpMode,
      fifo_q       => fifo_q,
      state_debug  => lcd_state_debug,
      fifo_rdusedw => fifo_rdusedw,
      fifo_rdreq   => fifo_rdreq,
      CSx          => CSx,
      WRx          => WRx,
      RDx          => RDx,
      D            => D);



  ----------------------------------------------------------------------------------------------------------------------
  -- Asymetric FIFO
  ----------------------------------------------------------------------------------------------------------------------

  fifo_data  <= AM_ReadData;
  fifo_rdclk <= Clk;
  fifo_wrclk <= Clk;
  fifo_wrreq <= AM_ReadValid;

  fifo_inst : entity work.fifo port map (data    => fifo_data,
                                         rdclk   => fifo_rdclk,
                                         rdreq   => fifo_rdreq,  --
                                         wrclk   => fifo_wrclk,
                                         wrreq   => fifo_wrreq,
                                         q       => fifo_q,      --
                                         rdusedw => fifo_rdusedw,
                                         wrusedw => fifo_wrusedw);

end architecture rtl;
