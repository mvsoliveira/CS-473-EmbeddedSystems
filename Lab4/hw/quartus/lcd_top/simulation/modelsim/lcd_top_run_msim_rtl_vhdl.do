transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {C:/cs473/Lab4/hw/hdl/lcd_control.vhd}
vcom -93 -work work {C:/cs473/Lab4/hw/hdl/lcd_top.vhd}

vcom -93 -work work {C:/cs473/Lab4/hw/quartus/lcd_top/../../modelsim/lcd_top_tb.vhd}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cyclonev -L rtl_work -L work -voptargs="+acc"  lcd_top_tb

add wave *
view structure
view signals
run -all
